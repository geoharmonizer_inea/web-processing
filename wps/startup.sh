#!/bin/bash

# WPS
envsubst '$NGINX_HTTP $NGINX_HOST $NGINX_PORT' < /opt/pywps/pywps.cfg.template > \
         /opt/pywps/pywps.cfg

# nginx
envsubst '$NGINX_HOST' < /etc/nginx/conf.d/default.conf.template > \
         /etc/nginx/conf.d/default.conf

# start nginx
nginx -g 'daemon off;' &

export LD_LIBRARY_PATH=/usr/lib/grass78/lib

gunicorn3 -b 0.0.0.0:8081 --workers $((2*`nproc --all`)) \
          --log-syslog  --pythonpath /opt/pywps pywps_app:application

exit 0
