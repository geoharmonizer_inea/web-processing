import os
import sys

import time
import types
import shutil
import magic
import logging
import math
from subprocess import PIPE

from grass.pygrass.modules import Module
from grass.exceptions import CalledModuleError

from pywps import Process, LiteralInput, ComplexOutput, LOGGER, FORMATS
from pywps.app.exceptions import ProcessError

from tools.aq_pm25_aggregate import pm25_aggregate, check_date


class AqPm25Aggregate(Process):
    def __init__(self):
        inputs = [
            LiteralInput(
                identifier="start_date", title="Start date", data_type="string"
            ),
            LiteralInput(identifier="end_date", title="End date", data_type="string"),
        ]
        outputs = [
            ComplexOutput(
                identifier="output",
                title="Output aggregated raster file",
                supported_formats=[FORMATS.GEOTIFF],
                as_reference=True,
            )
        ]

        super().__init__(
            self._handler,
            identifier="aq-pm-aggregate",
            version="0.1",
            title="Air Quality PM2.5 aggregation",
            abstract="Generates aggregated PM2.5 product for specified time window",
            inputs=inputs,
            outputs=outputs,
            grass_location="/data/grassdata/aq_location",
            store_supported=True,
            status_supported=True,
        )

        os.environ["GRASS_SKIP_MAPSET_OWNER_CHECK"] = "1"
        os.environ["HOME"] = "/tmp"  # needed by G_home()

    @staticmethod
    def _get_output_args(output_format, rescale_factor=None):
        output_args = {
            "flags": "m",  # no extra metadata
            "format": output_format,
            "type": "UInt16" if rescale_factor else "Byte",
            "createopt": "COMPRESS=DEFLATE",
        }
        if rescale_factor:
            scale = 1 / rescale_factor
            output_args["metaopt"] = f"SCALE_FACTOR={scale}"
            output_args["scale"] = scale

        return output_args

    def _export_output(
        self, input_name, output_name, output_format, output_rescale_factor
    ):
        # export rescaled data
        Module(
            "r.out.gdal",
            input=input_name,
            output=output_name,
            **self._get_output_args(output_format, output_rescale_factor),
        )

    def _handler(self, request, response):
        try:
            start_date = check_date(request.inputs["start_date"][0].data)
            end_date = check_date(request.inputs["end_date"][0].data)
        except ValueError as e:
            raise ProcessError(e)

        strds_input = "pm25_daily@PERMANENT"
        raster_output = "pm25_aggr"
        output_basename = "aq_pm25_et.eml_m_1km_na"
        output_postfix = "eumap_epsg3035_v0.1prebeta"
        color_table = os.path.join(os.path.dirname(__file__), "pm25_style.txt")
        output_rescale_factor = 100
        output_format = "GTiff"
        output_name = "{}_{}..{}_{}.tif".format(
            output_basename, start_date, end_date, output_postfix
        )

        # workaround https://gitlab.com/ctu-geoforall-lab/ctu-geoharmonizer/-/issues/118
        os.chdir(self.workdir)

        response.update_status(
            message='Computation progress',
            status_percentage=5
        )

        pm25_aggregate(
            start_date, end_date, strds_input, raster_output, output_rescale_factor
        )

        response.update_status(
            message='Computation progress',
            status_percentage=95
        )

        self._export_output(
            raster_output, output_name, output_format, output_rescale_factor
        )

        response.outputs["output"].file = output_name

        return response
