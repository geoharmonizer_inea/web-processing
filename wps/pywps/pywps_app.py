#!/usr/bin/env python3

__author__ = "Martin Landa"

import os
import sys

from pywps.app.Service import Service

os.environ['GISBASE'] = '/opt/grass79' 
sys.path.append(os.path.join(os.environ["GISBASE"], "etc", "python"))

from processes.aq_pm25_aggregate import AqPm25Aggregate

processes = [
    AqPm25Aggregate()
]

application = Service(
    processes,
    ['/opt/pywps/pywps.cfg']
)
