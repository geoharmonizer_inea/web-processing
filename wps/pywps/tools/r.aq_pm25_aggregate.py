#!/usr/bin/env python3

#%module
#% description: Computes Air Quality PM2.5 aggregation from daily products.
#%end
#%option
#% key: start_date
#% description: Start date
#% type: string
#% required: yes
#%end
#%option
#% key: end_date
#% description: End date
#% type: string
#% required: yes
#%end
#%option
#% key: pm_out
#% description: PM out
#% type: string
#% required: yes
#% answer: pm25_aggr
#%end


import os
from datetime import datetime
import sys

import grass.script as grass
from grass.pygrass.modules import Module


def check_date(date_str):
    try:
        date = datetime.strptime(date_str, "%Y-%m-%d").date()
    except ValueError:
        raise ValueError("Invalid date: {}".format(date_str))

    if date.year != 2018:
        raise ValueError("Valid range: 2018-01-01:2018-12-31")

    return date


def pm25_aggregate(
    start_date,
    end_date,
    strds_input="pm25_daily@PERMANENT",
    raster_output="pm25_aggr",
    output_rescale_factor=100,
):
    # copy mask from PERMANENT
    Module("g.copy", raster=["MASK@PERMANENT", "MASK"])

    # perform aggregation
    Module(
        "t.rast.series",
        input=strds_input,
        output=raster_output + "_fp",
        where="start_time >= '{}' and end_time <= '{}'".format(start_date, end_date),
    )

    # rescale data to int
    Module(
        "r.mapcalc",
        expression="{o} = round({i} * {r})".format(
            i=raster_output + "_fp", r=output_rescale_factor, o=raster_output
        ),
    )

    colors = """
        0    #163db0
        10   #163db0
        13   #176596
        16   #1f8f8d
        19   #23ba99
        22   #abdda4
        25   #f4c379
        28   #ff805d
        31   #f65737
        34   #c94839
        10000 #a91315
    """

    # set color table
    Module(
        "r.colors",
        map=raster_output,
        rules='-',
        stdin_=colors
    )


def main():
    pm25_aggregate(
        check_date(opt["start_date"]),
        check_date(opt["end_date"]),
        raster_output=opt["pm_out"]
    )


if __name__ == "__main__":
    opt, _ = grass.parser()
    sys.exit(main())
