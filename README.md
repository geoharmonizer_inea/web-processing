# Experimental Geo-harmonizer Web Processing Service

## Data preparation

```bash
nohup grass78 -c EPSG:3035 /data/grassdata/geoharmonizer_aq_wps --exec python3 import_pm_maps.py strds_inputs=pm_daily &
```

### Run GRASS tool locally

```bash
MAPSET_PATH=/data/grassdata/geoharmonizer_aq_wps/temp_$(cat /proc/sys/kernel/random/uuid)
grass78 -c $MAPSET_PATH --exec python3 pywps/tools/aq_pm25_aggregate.py start=2018-01-01 end=2018-01-15
```

Check result:

```bash
grass78 $MAPSET_PATH --exec r.info pm25_aggr -r
```

Delete mapset:

```bash
rm -rf $MAPSET_PATH
```

## PyWPS

### Build & Run

```bash
cd wps/
docker-compose build
docker-compose up -d
```

### Test

URL: http://localhost:8080/services/wps

[Capabilities](http://localhost:8080/services/wps?service=wps&version=1.0.0&request=GetCapabilities)

[DescribeProcess](http://localhost:8080/services/wps?service=wps&version=1.0.0&request=DescribeProcess&identifier=aq-pm-aggregate)

[Execute](http://localhost:8080/services/wps?service=wps&version=1.0.0&request=Execute&identifier=aq-pm-aggregate&datainputs=start_date=2018-01-01;end_date=2018-01-15)

## actinia

## Build and run the image in a detached mode

```bash
cd actinia
docker-compose -f docker/docker-compose.yml up -d
```

### Test

Run aggregation:

```bash
curl -u virginia-woolf:Waves -X POST -H "Content-Type: application/json" "http://localhost:8088/api/v1/locations/geoharmonizer/processing_async_export" -d @actinia/query_json.json | jq
```
